package lauriechanlam;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TriangleTest {


  @Test
  void tooManyArguments() {
    final String[] args = new String[]{"1", "2", "3", "4"};
    Throwable exception = assertThrows(IllegalArgumentException.class, () -> Triangle.getTypeFromArgs(args));
    assertEquals("expected 3 arguments, got 4", exception.getMessage());
  }

  @Test
  void notEnoughArguments() {
    final String[] args = new String[]{};
    Throwable exception = assertThrows(IllegalArgumentException.class, () -> Triangle.getTypeFromArgs(args));
    assertEquals("expected 3 arguments, got 0", exception.getMessage());
  }

  @Test
  void floatingPointArguments() {
    final String[] args = new String[]{"1", "2", "3.14"};
    Throwable exception = assertThrows(IllegalArgumentException.class, () -> Triangle.getTypeFromArgs(args));
    assertEquals("expected all edges to be positive integers, got 3.14", exception.getMessage());
  }

  @Test
  void negativeArguments() {
    final String[] args = new String[]{"-42", "2", "3"};
    Throwable exception = assertThrows(IllegalArgumentException.class, () -> Triangle.getTypeFromArgs(args));
    assertEquals("expected all edges to be positive integers, got -42", exception.getMessage());
  }

  @Test
  void notTriangle() {
    final String[] args = new String[]{"10", "1", "7"};
    Throwable exception = assertThrows(IllegalArgumentException.class, () -> Triangle.getTypeFromArgs(args));
    assertEquals("expected the triangle inequality to be respected, got 1 + 7 < 10", exception.getMessage());
  }

  @Test
  void equilateral() {
    final String[] args = new String[]{"0", "0", "0"};
    final String type = Triangle.getTypeFromArgs(args);
    assertEquals("equilateral", type);
  }

  @Test
  void isoscelesWithTwoFirst() {
    final String[] args = new String[]{"42", "42", "59"};
    final String type = Triangle.getTypeFromArgs(args);
    assertEquals("isosceles", type);
  }

  @Test
  void isoscelesWithTwoLast() {
    final String[] args = new String[]{"4", "42", "42"};
    final String type = Triangle.getTypeFromArgs(args);
    assertEquals("isosceles", type);
  }

  @Test
  void isoscelesWithFirstAndLast() {
    final String[] args = new String[]{"42", "44", "42"};
    final String type = Triangle.getTypeFromArgs(args);
    assertEquals("isosceles", type);
  }

  @Test
  void scalene() {
    final String[] args = new String[]{"4", "1", "3"};
    final String type = Triangle.getTypeFromArgs(args);
    assertEquals("scalene", type);
  }
}
