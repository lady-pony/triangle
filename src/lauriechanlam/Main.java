package lauriechanlam;

public class Main {
  public static void main(String[] args) {
    try {
      String type = Triangle.getTypeFromArgs(args);
      String message = String.format("The triangle with edges (%s, %s, %s) is %s", args[0], args[1], args[2], type);
      System.out.println(message);
    } catch (IllegalArgumentException e) {
      System.err.println(e.getMessage());
    }
  }
}
