# Triangle

## Assignment

Write a program that will determine the type of a triangle. It should take the
lengths of the triangle's three sides as input, and return whether the triangle
is equilateral, isosceles or scalene.

We are looking for solutions that showcase problem solving skills and structural
considerations that can be applied to larger and potentially more complex
problem domains. Pay special attention to tests, readability of code and error cases.

The way you reflect upon your decisions is important to us, why we ask you to
include a brief discussion of your design decisions and implementation choices.

The resulting code and discussion is vital for us and will be used as a way for
us to validate your engineering skills. After having reviewed your code, we’ll
decide on next step.

Please put the solution up on GitHub and send the link to me.

## Analysis

1. **Output type** Equilateral / Isocele / Scalene is a partition of the space
   of triangles. Each triangle is exactly one of those. Thus, the function
   I will write will return `enum TRIANGLE_TYPE { EQUILATERAL, ISOCELE, SCALENE }`.
2. **Input type** This is a constraint of the problem. We have a list of three lengths.
   In order to simplify the problem, we will assume that the lengths are integers
   representing the same unit (centimeters for example).
   Indeed, floating point numbers would be hard to compare. And what if the length
   is actually [ sqrt(2), sqrt(2), sqrt(2) ]. Then the floating point representation
   is just an approximation of the actual value.
3. **Program I/O** The program will be a command line tool. It will take three
   integer arguments. If the input is invalid, it should display an error message.
   If the input is valid, it should display a message like `The triangle with edges
   of lengths 1, 1, 1 is equilateral`.
4. **Function signature** will be something like
   `TRIANGLE_TYPE getTriangleType(unsigned int [3] edges)`
5. As we have to deal with a triangle, it is fine to copy the data from one structure
   to another, we have only 3 integers to copy.
6. I haven't worked with Java for a while, so I implement the solution in Java in
   order to showcase that this is not an issue given that I have developed a lot in
   C++ and JavaScript and the programming language is a tool that can be learnt.

## Test cases

1. **Invalid inputs**
   * Too many edges
   * Not enough edges
   * Non integers edges
   * Negative edges
   * Three edges that are not a triangle
2. **Valid inputs**
   * 3 equal values
   * 2 first values are equal, 3rd one is different
   * 2 last values are equal, 1st one is different
   * 1st and last values are equal, 2nd one is different
   * all values are different

## Implementation

As the coding language is mostly Java based (or Scala, or Clojure), I choose to
implement the solution in Java. The tests will be implemented in JUnit.

The types of triangles we are looking for directly map with the number of
edges with equal length. This also directly maps with the size of a set
that contains the values of the lengths.

## Instructions

### Prerequisites

* JAVA
* Developed on Mac (not tested on Windows or other platforms)

### Command lines

* **make build** in order to build the project. Source code is available in [src](./src/lauriechanlam)
* **make test** in order to run the unit tests. Source code is available in [test](./test/lauriechanlam/TriangleTest.java)
* **make clean** in order to clean the build.

In order to execute the program, run
```console
$ ./scripts/run.sh <arguments>
```

The type of triangle will be shown in the CLI. For example
```console
$ ./scripts/run.sh 3 5 4
./scripts/build.sh
The triangle with edges (3, 5, 4) is scalene
```
