#!/bin/bash

build:
	./scripts/build.sh

test: build
	./scripts/test.sh

clean:
	rm -rf bin

